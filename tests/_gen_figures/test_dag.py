"""DAG-related examples that are *not* using the plotting framework"""

import os

from dantro.dag import TransformationDAG
from dantro.tools import load_yml

from .._fixtures import *
from ..test_dag import TRANSFORMATIONS_PATH, create_dm

# -----------------------------------------------------------------------------


def test_dag_vis(out_dir):
    """Creates output from DAG doc_examples"""
    dm = create_dm()

    to_plot = (
        "meta_ops_deeply_nested",
        "doc_examples_define",
        "doc_examples_err_and_fallback",
        "doc_examples_select_with_transform",
        "doc_examples_op_hooks_expression_symbolic",
        "doc_examples_meta_ops_prime_multiples",
        "doc_examples_meta_ops_multiple_return_values",
    )

    vis_params = dict(
        generation=dict(include_results=True),
    )

    for cfg_name, cfg in load_yml(TRANSFORMATIONS_PATH).items():
        if cfg_name not in to_plot:
            continue

        print(f"... Case: '{cfg_name}' ...")

        tdag = TransformationDAG(dm=dm, **cfg["params"])
        try:
            tdag.compute(compute_only=cfg.get("compute_only", "all"))
        except:
            if not cfg.get("_raises_on_compute"):
                # Should not have raised
                raise
            pass

        tdag.visualize(
            out_path=os.path.join(out_dir, f"{cfg_name}.pdf"),
            **vis_params,
        )
